import { action, observable } from 'mobx';
import * as Request from 'request-promise';
import * as Cookies from 'es-cookie';

// const domain = 'http://dev-space-8604.lshackathon.dev.covexo.com/api/v1/';
export const domain = '/api/v1';

export interface Location {
    longitude: number;
    latitude: number;
}

export class UserModel {
    @observable token: string;
    sex: string;
    birthday: Date;
    pregnant: false;
    location: Location;

    @observable doctor: boolean;

    @action
    request() {
        return fetch(domain + '/patient', {
            method: 'GET',
            cache: 'no-cache',
            credentials: 'include',
            headers: {
                'patient-token': this.token
            },
            mode: 'cors'
        })
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.sex = data.gender;
                this.birthday = new Date(data.birthday);
                this.pregnant = data.isPregnant;
                return null;
            });
    }

    @action
    init(sex: string, birthday: Date, location: Location, pregnent: boolean) {
        fetch(domain + '/patient/register', {
            method: 'POST',
            body: JSON.stringify({
                gender: sex,
                birthday: birthday.toJSON(),
                location: {
                    lat: location.latitude + Math.random() * 0.01 - 0.01,
                    lang: location.longitude + Math.random() * 0.01 - 0.01
                },
                isPregnant: pregnent,
                lastLocationDate: new Date().toJSON()
            }),
            cache: 'no-cache',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff'
            },
            mode: 'cors'
        })
            .then(res => {
                return res.json();
            })
            .then((data: { patientToken: string }) => {
                this.token = data.patientToken;
                Cookies.set('token', data.patientToken);
            });
    }
}
