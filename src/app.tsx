import './app.scss';
import * as React from 'react';

/* Import: helpers */
import { observer } from 'mobx-react';
import * as Cookies from 'es-cookie';

/* Import: components  */
import { Button } from '@material-ui/core';
import { LoginView } from './views/Login/loginView';
import { PatientMap } from './views/PatientMap/patientMap';
import { createMuiTheme } from '@material-ui/core/styles';
import { UserModel } from './model/rootModel';
import { MedicalReportView } from './views/MedicalReport/medicalReportView';

@observer
export class App extends React.Component {
    private user = new UserModel();

    constructor(props: null) {
        super(props);

        const token: string = Cookies.get('token');
        if (token) this.user.token = token;
    }

    render() {
        return (
            <div className={'app'}>
                {!this.user.token ? (
                    this.user.doctor ? (
                        <PatientMap />
                    ) : (
                        <LoginView user={this.user} />
                    )
                ) : (
                    <MedicalReportView user={this.user} />
                )}
            </div>
        );
    }
}
