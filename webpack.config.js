const path = require('path');
const webpack = require('webpack');
const Stylish = require('webpack-stylish');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const {
    CheckerPlugin,
    TsConfigPathsPlugin
} = require('awesome-typescript-loader');

module.exports = {
    mode: 'development',
    devtool: 'source-map',

    entry: {
        app: './src/index.tsx'
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'public/js'),
        crossOriginLoading: 'use-credentials'
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.scss'],
        plugins: [new TsConfigPathsPlugin /* { configFileName, compiler } */()]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: [
                                path.resolve(__dirname, 'src/global/scss')
                            ]
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new Stylish(),
        new CheckerPlugin()
    ],

    devServer: {
        quiet: true,
        open: false,
        contentBase: path.resolve(__dirname, 'public'),
        historyApiFallback: true,
        port: 8080,
        overlay: {
            warnings: true,
            errors: true
        },
        proxy: {
            '/api': {
                target: 'http://meco.fwd.wf',
                changeOrigin: true,
                secure: false
            }
        }
    }
};
